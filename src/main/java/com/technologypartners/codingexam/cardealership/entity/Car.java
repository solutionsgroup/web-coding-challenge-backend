package com.technologypartners.codingexam.cardealership.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Car {

    @Id
    @GeneratedValue
    private Long id;

    private Integer year;

    private String make;

    private String model;
}
