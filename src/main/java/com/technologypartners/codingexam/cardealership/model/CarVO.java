package com.technologypartners.codingexam.cardealership.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CarVO {

    private Long id;

    private Integer year;

    private String make;

    private String model;

    @Builder
    public CarVO(Long id, Integer year, String make, String model) {
        this.id = id;
        this.year = year;
        this.make = make;
        this.model = model;
    }
}
