package com.technologypartners.codingexam.cardealership.service;

import com.technologypartners.codingexam.cardealership.model.CarVO;
import com.technologypartners.codingexam.cardealership.repository.CarRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CarService {

    private CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<CarVO> findAllCars() {
        return carRepository.findAll().stream().map(car ->
                CarVO.builder()
                        .id(car.getId())
                        .make(car.getMake())
                        .model(car.getModel())
                        .year(car.getYear())
                        .build()).collect(Collectors.toList());
    }
}
