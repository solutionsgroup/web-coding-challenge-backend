package com.technologypartners.codingexam.cardealership.controller;

import com.technologypartners.codingexam.cardealership.model.CarVO;
import com.technologypartners.codingexam.cardealership.service.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CarController {

    // TODO: Wire what you need to obtain the data for the find method below.

    // TODO: Complete this REST URI path.
    @GetMapping(path = "/V1/api")
    @CrossOrigin
    public ResponseEntity<List<CarVO>> findAllCars() {
        // TODO: Replace the null below with the appropriate method call to return the data.
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
}
