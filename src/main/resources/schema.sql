CREATE TABLE car (
  id int auto_increment,
  year int,
  make varchar,
  model varchar,
  PRIMARY KEY (id)
);
